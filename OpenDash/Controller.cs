﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenDash.Adapters;
using OpenDash.Devices;
using OpenDash.Devices.Serial;
using OpenDash.Serial.Delegates;
using OpenDash.Adapters.Delegates;
using OpenDash.DataStructure;

namespace OpenDash
{
    class Controller
    {
        AdapterManager am = new AdapterManager();
        DeviceManager dm = new SerialManager();

        SimFoundEventHandler OnSimFoundDelegate;
        DeviceFoundHandler OnDeviceFoundDelegate;
        DeviceDisconnectedHandler OnDeviceDisconnected;
        

        public void HandleUpdate(Data update)
        {
            dm.HandleUpdate(update);
        }

        public Controller()
        {
            am.OnSimFoundDelegate += OnSimFound;
        }

        public void ConnectToDevice(Device device)
        {
            dm.AddDevice(device);
            device.Connect();
        }

        public void AddOnUpdateHandler()
        {

        }

        public void StartScanningForDevices(int interval)
        {
            dm.DeviceFound += OnDeviceFound;
            dm.StartScanning(interval);
        }

        public void StartScanningForSim()
        {
            am.OnSimFoundDelegate += (new SimFoundEventHandler(OnSimFound));
            am.StartScan();
        }

        public void OnSimFound(object sender, EventArgs args)
        {
            OnSimFoundDelegate(sender, args);
            am.AddOnUpdateHanler(new UpdatedDataEventHandler(OnDataUpdated));
            am.StartUpdating();
        }

        public void OnDataUpdated(Data data)
        {
            dm.HandleUpdate(data);
        }

        public void AddDeviceFoundHandler(DeviceFoundHandler handler)
        {
            OnDeviceFoundDelegate += handler;
        }

        public void OnDeviceFound(string id)
        {
            OnDeviceFoundDelegate(id);
        }

        public void AddDeviceDisconnectedHandler()
        {

        }

        public void AddSimFoundHandler(SimFoundEventHandler handler)
        {
            OnSimFoundDelegate += handler;
        }
        
        public string GetSimName()
        {
            return am.GetNameSimInUse();
        }
    }
}
