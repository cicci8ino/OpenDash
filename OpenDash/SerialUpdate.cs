﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenDash.Serial
{
    class SerialUpdate
    {
        internal byte type;
        internal List<Byte> bytes;

        internal byte getType()
        {
            return type;
        }

        internal int getLenght()
        {
            return bytes.Count;
        }

        public SerialUpdate(byte type, short value)
        {
            this.type = type;
            bytes = new List<Byte>(BitConverter.GetBytes(value));
            
        }

        public SerialUpdate(byte type, sbyte value)
        {
            this.type = type;
            bytes = new List<Byte>(BitConverter.GetBytes(value));
        }

        public SerialUpdate(byte type, int value)
        {
            this.type = type;
            bytes = new List<Byte>(BitConverter.GetBytes(value));
        }

        public SerialUpdate(byte type, long value)
        {
            this.type = type;
            bytes = new List<Byte>(BitConverter.GetBytes(value));
        }

        public SerialUpdate(byte type, float value)
        {
            this.type = type;
            bytes = new List<Byte>(BitConverter.GetBytes(value));
        }
    }

    
}
