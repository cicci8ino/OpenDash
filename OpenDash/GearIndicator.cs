﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenDash.DataStructure;

namespace OpenDash.Devices.Serial
{
    class GearIndicator : SerialDevice
    {
        public GearIndicator(string portName, int baudRate) : base(portName, baudRate)
        {
        }

        public override string GetName()
        {
            return "Gear Indicator";
        }

        public override void OnUpdate()
        {
            SendGear();
            //Console.WriteLine("YOLO");
        }

        
    }
}
