﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenDash.Serial.Delegates
{
    public delegate void DeviceFoundHandler(String port);
    public delegate void DeviceDisconnectedHandler(String port);
}
