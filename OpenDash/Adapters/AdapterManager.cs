﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenDash.DataStructure;
using OpenDash.Adapters.Delegates;
using OpenDash.Adapters.rF2;

namespace OpenDash.Adapters
{
    class AdapterManager
    {
        public Adapter SimInUse;

        public UpdatedDataEventHandler OnUpdateDelegate;
        public SimFoundEventHandler OnSimFoundDelegate;
        public SimDisconnectedEventHandler OnSimDisconnectedDelegate;

        public List<Adapter> AdapterList = new List<Adapter>();

        public AdapterManager()
        {
            AddKnownSim();
        }

        public AdapterManager(int scanInterval, bool autoReconnect, int updateInterval)
        {

        }

        private void AddKnownSim()
        {
            AdapterList.Add(new rF2Adapter());
        }

        public string GetNameSimInUse()
        {
            return SimInUse.GetSimName() + "\nAdapter version" + SimInUse.GetVersion();
        }

        public void StartScan()
        {
            foreach (Adapter adapter in AdapterList)
            {
                adapter.AddOnFoundHandler(HandleOnSimFound);
                //adapter.AddOnUpdateHandler(OnUpdate);
                adapter.StartScanning();
                Console.WriteLine("Scan started for " + adapter.GetSimName());
            }
        }

        public void StartUpdating()
        {
            SimInUse.StartUpdating();
        }

        public void StopScan()
        {
            foreach (Adapter adapter in AdapterList)
            {
                adapter.StopScan();
            }
        }

        private void HandleOnSimFound(object sender, EventArgs args)
        {
            StopScan();

            SimInUse = (Adapter)sender;
            SimInUse.Connect();

            System.Console.WriteLine(SimInUse.GetSimName() + " found.");
            OnSimFoundDelegate(SimInUse, null);
                //OnSimFound(SimInUse, null);
        }

        public void AddOnUpdateHanler(UpdatedDataEventHandler handler) //wrong name
        {
            //Console.WriteLine("added handler");
            SimInUse.AddOnUpdateHandler(handler);
        }

        public void HandleOnSimDisconnect()
        {
            StartScan();
        }

    }
}
