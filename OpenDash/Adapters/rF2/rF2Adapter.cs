﻿using System;
using System.Collections.Generic;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using OpenDash.Adapters.rF2.SharedMemory.rFactor2Data;
using OpenDash.DataStructure;
using System.IO;
using System.Threading;

namespace OpenDash.Adapters.rF2
{
    class rF2Adapter : Adapter
    {
        private MemoryMappedFile memoryMappedFile1 = null;
        private MemoryMappedFile memoryMappedFile2 = null;
        private readonly int SHARED_MEMORY_SIZE_BYTES = Marshal.SizeOf(typeof(rF2State));
        private readonly int SHARED_MEMORY_HEADER_SIZE_BYTES = Marshal.SizeOf(typeof(rF2StateHeader));
        private byte[] sharedMemoryReadBuffer = null;
        private int currBuff = 0;


        rF2State currrF2State;
        private int updateInterval = 30;

        String SimName = "rFactor 2";
        String PluginVersion = "v0.1 alpha";

        public Data GetData(object simUpdate)
        {
            rF2State currentState = (rF2State)simUpdate;
            Data update = new Data();
            update.vehicle.currentGear = (byte)currentState.mGear;
            update.vehicle.engine.engineRPM = (short)currentState.mEngineRPM;
            update.vehicle.engine.engineRPMMax = (short)currentState.mEngineMaxRPM;
            update.vehicle.engine.engineOilTemp = (float)Math.Round(currentState.mEngineOilTemp, 0);
            update.vehicle.engine.engineWaterTemp = (float)Math.Round(currentState.mEngineWaterTemp, 0);

            return update;
        }



        public override object GetRawData(string rawData)
        {
            throw new NotImplementedException();
        }

        public override void Connect()
        {
            try
            {
                this.memoryMappedFile1 = MemoryMappedFile.OpenExisting(SharedMemory.rFactor2Constants.MM_FILE_NAME1);
                this.memoryMappedFile2 = MemoryMappedFile.OpenExisting(SharedMemory.rFactor2Constants.MM_FILE_NAME2);
                this.sharedMemoryReadBuffer = new byte[this.SHARED_MEMORY_SIZE_BYTES];
                base.IsConnected = true;
                //OnConnect(this, null);

            }
            catch (Exception)
            {
                OnCannotConnect(this, null);
            }
        }

        public override void Disconnect()
        {
            this.memoryMappedFile1.Dispose();
            this.memoryMappedFile2.Dispose();
            base.IsConnected = false;
            OnDisconnect(this, null);
        }

        public override void RetrieveUpdate()
        {
            try
            {
                // Note: if it is critical for client minimize wait time, same strategy as plugin uses can be employed.
                // Pass 0 timeout and skip update if someone holds the lock.
                try
                {
                    bool buf1Current = false;
                    // Try buffer 1:
                    using (var sharedMemoryStreamView = this.memoryMappedFile1.CreateViewStream())
                    {
                        var sharedMemoryStream = new BinaryReader(sharedMemoryStreamView);
                        this.sharedMemoryReadBuffer = sharedMemoryStream.ReadBytes(this.SHARED_MEMORY_HEADER_SIZE_BYTES);

                        // Marhsal header
                        var headerHandle = GCHandle.Alloc(this.sharedMemoryReadBuffer, GCHandleType.Pinned);
                        var header = (rF2StateHeader)Marshal.PtrToStructure(headerHandle.AddrOfPinnedObject(), typeof(rF2StateHeader));
                        headerHandle.Free();

                        if (header.mCurrentRead == 1)
                        {
                            sharedMemoryStream.BaseStream.Position = 0;
                            this.sharedMemoryReadBuffer = sharedMemoryStream.ReadBytes(this.SHARED_MEMORY_SIZE_BYTES);
                            buf1Current = true;
                            this.currBuff = 1;
                            //  Console.WriteLine("Buffer 1");
                        }

                    }

                    // Read buffer 2
                    if (!buf1Current)
                    {
                        using (var sharedMemoryStreamView = this.memoryMappedFile2.CreateViewStream())
                        {
                            var sharedMemoryStream = new BinaryReader(sharedMemoryStreamView);
                            this.sharedMemoryReadBuffer = sharedMemoryStream.ReadBytes(this.SHARED_MEMORY_SIZE_BYTES);
                            this.currBuff = 2;
                        }
                        //        Console.WriteLine("Buffer 2");
                    }

                }
                finally
                {

                }

                // Marshal rF2State
                var handle = GCHandle.Alloc(this.sharedMemoryReadBuffer, GCHandleType.Pinned);
                this.currrF2State = (rF2State)Marshal.PtrToStructure(handle.AddrOfPinnedObject(), typeof(rF2State));
                handle.Free();
                OnUpdate(GetData(this.currrF2State));
                Thread.Sleep(updateInterval);

            }
            catch (Exception)
            {
                OnDisconnect(this, null);
                //SetDataInvalid();
                //return new Data();
            }
        }

        protected override void Scan()
        {
                Console.WriteLine("Scanning");
                try
                {
                    MemoryMappedFile.OpenExisting(SharedMemory.rFactor2Constants.MM_FILE_NAME1);
                    MemoryMappedFile.OpenExisting(SharedMemory.rFactor2Constants.MM_FILE_NAME2);
                    this.NeedToScan = false;
                    OnProcessFound(this, null);
                    
                }
                catch (Exception)
                {

                }
                Thread.Sleep(1000);
        }

        public override string GetSimName()
        {
            return SimName;
        }

        public override string GetVersion()
        {
            return PluginVersion;
        }


    }
}
