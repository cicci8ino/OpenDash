﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenDash.DataStructure;

namespace OpenDash.Adapters.Delegates
{
    public delegate void UpdatedDataEventHandler(Data update);

    public delegate void DisconnectedEventHandler(object sender, EventArgs args);

    public delegate void ConnectedEventHandler(object sender, EventArgs args);

    public delegate void CannotConnectEventHandler(object sender, EventArgs args);

    public delegate void SimFoundEventHandler(object sender, EventArgs args);

    public delegate void SimDisconnectedEventHandler(object sender, EventArgs args);

    public delegate void ReadyToConnectHandler(object sender, EventArgs args);
}
