﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenDash.Adapters.Delegates;
using System.Threading;
using OpenDash.DataStructure;

namespace OpenDash.Adapters
{
    abstract class Adapter
    {
        private Thread updateThread;
        private Thread scanThread;
        protected bool NeedToScan = true;
    

        protected UpdatedDataEventHandler OnUpdate;
        protected DisconnectedEventHandler OnDisconnect;
        protected ConnectedEventHandler OnConnect;
        protected CannotConnectEventHandler OnCannotConnect;
        protected SimDisconnectedEventHandler OnProcessClosed;
        protected SimFoundEventHandler OnProcessFound;
        protected ReadyToConnectHandler OnReadyToConnect;

        public abstract String GetSimName();
        public abstract String GetVersion();

        protected bool IsConnected;

        public abstract void RetrieveUpdate();
        public abstract void Connect();
        public abstract void Disconnect();

        public void StopScan()
        {
            NeedToScan = false;
        }
        public void StartScanning()
        {
            scanThread = new Thread(ScanForSim);
            scanThread.IsBackground = true;
            scanThread.Start();
        }

        protected abstract void Scan();

        private void ScanForSim()
        {
            while (NeedToScan)
                Scan();
        }

        private void Update()
        {
            while (IsConnected) {
                RetrieveUpdate();
            }
        }

        public void StartUpdating()
        {
            updateThread = new Thread(Update);
            updateThread.IsBackground = true;
            updateThread.Start();
        }

        public abstract object GetRawData(String rawData);

        #region Events Handlers

        public void AddOnUpdateHandler(UpdatedDataEventHandler handler)
        {
            OnUpdate += handler;
        }

        public void AddOnDisconnectHandler(DisconnectedEventHandler handler)
        {
            OnDisconnect += handler;
        }

        public void AddOnConnectHandler(ConnectedEventHandler handler)
        {
            OnConnect += handler;
        }

        public void AddOnConnectionErrorHandler(CannotConnectEventHandler handler)
        {
            OnCannotConnect += handler;
        }

        public void AddOnFoundHandler(SimFoundEventHandler handler)
        {
            OnProcessFound += handler;
        }

        /*

        public void AddOnProcessClosedHandler(ProcessClosedEventHandler handler)
        {
            OnProcessClosed += handler;
        }

        public void AddOnProcessFoundHandler(ProcessFoundEventHandler handler)
        {
            OnProcessFound += handler;
        } */

        public void AddOnReadyToConnectHandler(ReadyToConnectHandler handler)
        {
            OnReadyToConnect += handler;
        }

        #endregion
    }

}
