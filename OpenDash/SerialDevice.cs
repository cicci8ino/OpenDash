﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using OpenDash.DataStructure;
using System.Threading;
using CommandMessenger.Transport.Serial;


namespace OpenDash.Old.Devices
{
    /*
    abstract class SerialDevice
    {

        public enum Constants : byte
        {
            NOT_USED,
            ENGINE_RPM,
            SPEED,
            CURRENT_GEAR,
            ENGINE_WATER_TEMP,
            ENGINE_OIL_TEMP,
            FUEL_LEFT,
            OVERHEATING,
            MAX_ENGINE_RPM,
            TOTAL_FUEL,
            BEST_PERSONAL_LAPTIME,
            CURRENT_LAPTIME
        }

        public SerialPort port;

        byte typeStart = 255;
        byte dataStart = 254;

        List<Byte> buffer = new List<Byte>();

        Thread sendingThread ;

        abstract public void update(Data update);

        public SerialDevice(string portName, int baudRate)
        {
            port = new SerialPort(portName, baudRate);
            port.Open();
            sendingThread = new Thread(syncSendData);
        }
 

        private void addToBuffer(SerialUpdate update)
        {
            buffer.Add(typeStart);
            buffer.Add(update.getType());
            buffer.Add(dataStart);
            foreach (Byte dataByte in update.bytes)
            {
                buffer.Add(dataByte);
            }
            //buffer.Add(valueDelimiterEnd);
        }

        public void syncSendData()
        {
            
            port.Write(buffer.ToArray(), 0, buffer.Count);
            buffer.Clear();
        }

        public void sendData()
        {
            /*
            System.Console.WriteLine("YOLO");
            sendingThread.IsBackground = false;
            sendingThread.Start();
            port.Write(buffer.ToArray(), 0, buffer.Count);
            buffer.Clear();
        }

        #region Send Value

        public void addGear(byte gear)
        {
           addToBuffer(new SerialUpdate((byte)Constants.CURRENT_GEAR, gear));
            
        }

        public void addRPM(short engineRPM)
        {
            addToBuffer(new SerialUpdate((byte)Constants.ENGINE_RPM, engineRPM));
        }

        public void addMaxRPM(short RPM)
        {
            addToBuffer(new SerialUpdate((byte)Constants.MAX_ENGINE_RPM, RPM));
        }

        public void addSpeed(short speed)
        {
            addToBuffer(new SerialUpdate((byte)Constants.SPEED, speed));
        }

        public void addCurrentLap(int lapTime)
        {
            addToBuffer(new SerialUpdate((byte)Constants.CURRENT_LAPTIME, lapTime));
        }

        public void addFuelLeft(float fuelLeft)
        {
            addToBuffer(new SerialUpdate((byte)Constants.FUEL_LEFT, fuelLeft));
        }

        public void addPersonalBestLapTime(int lapTime)
        {
            addToBuffer(new SerialUpdate((byte)Constants.BEST_PERSONAL_LAPTIME, lapTime));
        }

        public String getLaptimeString(double laptime)
        {
            TimeSpan laptimeT = TimeSpan.FromMilliseconds(laptime);
            int minutes = laptimeT.Minutes;
            int seconds = laptimeT.Seconds;
            int ms = laptimeT.Milliseconds;
            return laptimeT.ToString(@"mm\:ss\:fff");
        }

        public void addBrakeBias(float brakeBias)
        {
           // addToBuffer(new SerialUpdate(SerialConstant.))
        }

        /*
        public void sendTotalFuel(float totalFuel)
        {
            short fuelValue = (short)(totalFuel * 10 + 0.001);
            bufferSerialUpdate[0] = Constant.SerialConstant.totalFuel;
            addValueToBuffer(fuelValue);
            sendData(bufferSerialUpdate, 3);
        }
        
        #endregion
    } */



}
