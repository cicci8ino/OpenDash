﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenDash.DataStructure

{
    public struct Data
    {
        internal Vehicle vehicle;
        internal Delta delta;
        internal LapInfo lap;
        internal SessionTime session;
        internal Weather weather;
    }
    
    struct Weather
    {

    }

    struct Delta
    {
        internal double timeDeltaLeader;
        internal double timeDeltaAhead;
        internal double timeDeltaBehind;
        internal double timeDeltaBest;
    }

    struct LapInfo
    {
        internal Laptime currentLaptime; //
        internal Laptime personalBest;
        internal Laptime sessionBest;
        internal Laptime virtualBest;

        internal string sessionBestAuthor;
        internal short currentLap;
        internal int position;

        internal short totalLap;
        internal double remainingTime;

    }

    struct SessionTime
    {
        internal byte sessionIndex;
        internal double remainingTime;
        internal double elapsedTime;
    }

    struct Vehicle
    {
        internal Engine engine; //
        internal Fuel fuel; //

        internal short speed; //
        internal byte currentGear;   // //0 for N, -1 for R

        internal Wheel rl;
        internal Wheel rr;
        internal Wheel fl;
        internal Wheel fr;

        internal float brakeBias; //

        internal bool DRS;
        internal bool headlights;
        internal bool speedLimiter;
        internal byte abs;
        internal byte TC;
        internal bool overHeating;

    }

    struct Laptime
    {
        internal double s1Time;
        internal double s2Time;
        internal double s3Time;
    }

    struct Fuel
    {
        internal float fuelLeft;
        internal float totalFuel;
    }

    struct Engine
    {
        internal float engineOilTemp; //
        internal float engineWaterTemp; //
        internal short engineRPM; //
        internal short engineRPMMax; //
    }

    struct Wheel
    {
        internal float grip;
        internal float[] temperature;
        internal float pressure;
        internal float brakeTemperature;
    }
}
