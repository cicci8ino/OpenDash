﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommandMessenger;
using OpenDash.DataStructure;
using CommandMessenger.Transport;

namespace OpenDash.Devices

{

    public enum Command 
    {
        NOT_USED,
        ENGINE_RPM,
        SPEED,
        CURRENT_GEAR,
        ENGINE_WATER_TEMP,
        ENGINE_OIL_TEMP,
        FUEL_LEFT,
        OVERHEATING,
        MAX_ENGINE_RPM,
        TOTAL_FUEL,
        BEST_PERSONAL_LAPTIME,
        CURRENT_LAPTIME
    }

    abstract class Device
    {

        

        public CmdMessenger cmd;
        private Data lastMessage;
        private Data oldMessage;
        private bool firstReceived = false;

        public void HandleUpdate(Data update)
        {
            lastMessage = update;
            OnUpdate();
            if (firstReceived==false)
                firstReceived = true;
        }

        public void Disconnect()
        {
            cmd.Disconnect();
        }

        public void Connect()
        {
            cmd.Connect();
        }

        public abstract string GetId();

        public abstract void OnUpdate();

        public abstract string GetName();

        public void AttachTransportLayer(ITransport transport)
        {
            cmd = new CmdMessenger(transport);
        }


        #region Send Functions

        public void SendSpeed()
        {
            if (lastMessage.vehicle.speed != oldMessage.vehicle.speed)
            {
                SendCommand cmd = new SendCommand((int)Command.SPEED);
                cmd.AddBinArgument(lastMessage.vehicle.currentGear);
                oldMessage.vehicle.speed = lastMessage.vehicle.speed;
                
            }
        }

        public void SendRPM()
        {

        }

        public void SendRPMMax()
        {

        }
        /*
        public void SendCommand(Command commandIndex, short var )
        {
            SendCommand cmd = new SendCommand((int)commandIndex);
            cmd.AddBinArgument(var);
            this.cmd()
        }

        public void SendCommand(Command commandIndex, float var)
        {
            SendCommand cmd = new SendCommand((int)commandIndex);
            cmd.AddBinArgument(var);
        }

        public void SendCommand(Command commandIndex, int var)
        {
            SendCommand cmd = new SendCommand((int)commandIndex);
            cmd.AddBinArgument(var);
        }*/

        public void SendGear()
        {
            {
                if (lastMessage.vehicle.currentGear != oldMessage.vehicle.currentGear || !firstReceived)
                {
                    SendCommand cmd = new SendCommand((int)Command.CURRENT_GEAR);
                    cmd.AddBinArgument(lastMessage.vehicle.currentGear);
                    oldMessage.vehicle.currentGear = lastMessage.vehicle.currentGear;
                    this.cmd.SendCommand(cmd);
                    
                }
            }
        }

        public void SendFuelLeft()
        {

        }

        public void SendFuelCapacity()
        {

        }

        public void SendEngineOilTemp()
        {

        }

        public void SendEngineWaterTemp()
        {

        }

        public void SendBrakeBias()
        {

        }

        public void SendCurrentS1()
        {

        }

        public void SendCurrentS2()
        {

        }

        public void SendCurrentS3()
        {

        }

        public void SendPosition()
        {

        }

        #endregion 
    }
}
