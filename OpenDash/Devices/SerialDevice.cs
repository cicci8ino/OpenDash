﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommandMessenger.Transport.Serial;
using CommandMessenger;

namespace OpenDash.Devices
{
    abstract class SerialDevice : Device
    {

        private string port;

        public SerialDevice(String port, int baudRate)
        {
            SerialTransport serialTransport = new SerialTransport
            {
                CurrentSerialSettings = { PortName = port, BaudRate = baudRate } // object initializer
            };
            base.AttachTransportLayer(serialTransport);
            
            this.port = port;
        }

        
        public abstract override string GetName();
        /*
        public override void OnUpdate()
        {
            throw new NotImplementedException();
        }*/

        public override abstract void OnUpdate();

        public override string GetId()
        {
            return port;
        }

    }
}
