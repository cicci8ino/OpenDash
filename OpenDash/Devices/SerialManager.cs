﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OpenDash.Devices
{
    class SerialManager : DeviceManager
    {

        private List<String> PortList = new List<String>();
        private List<String> PreviousPort = new List<String>();
        /*
        public override void AddDevice(string id)
        {
            throw new NotImplementedException();
        }
        */

        public override void RetrieveDevice()
        {
            string[] found = SerialPort.GetPortNames();
            foreach (string port in found)
            {
                if (!PreviousPort.Contains(port))
                    OnNewDeviceFound(port);
                PortList.Add(port);
            }
            foreach (string port in PreviousPort)
            {
                if (!PortList.Contains(port))
                {
                    OnDeviceDisconnect(port);
                }
            }

            PreviousPort.Clear();
            foreach (string port in PortList)

            {
                PreviousPort.Add(port);
            }
            PortList.Clear();
            Thread.Sleep(ScanningTime);
        }

        public override void OnNewDeviceFound(string id)
        {
            PortList.Add(id);
            PreviousPort.Add(id);
            System.Console.WriteLine(id + " Found");
            DeviceFound(id);
        }

        
    }
}
