﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenDash.Serial;
using System.Threading;
using System.IO.Ports;
using OpenDash.Serial.Delegates;
using OpenDash.DataStructure;

namespace OpenDash.Devices
{
    abstract class DeviceManager
    {

        public DeviceFoundHandler DeviceFound;
        public DeviceDisconnectedHandler DeviceDisconnected;

        public Thread ScanningThread;
        public int ScanningTime;
        public bool IsScanning = false;

        private Dictionary<string, Device> DeviceMap = new Dictionary<string, Device>();
       // private List<Device> DeviceList = new List<Device>();

       // public abstract void AddDevice(String id);

        public void AddDevice(Device device)
        {
            DeviceMap.Add(device.GetId(), device);
            Console.WriteLine(device.GetId() + " added");
        }

        public void RemoveDevice(String id)
        {
            Device device;
            if (DeviceMap.TryGetValue(id, out device))
            {
                device.Disconnect();
                DeviceMap.Remove(id);
                Console.WriteLine(device.GetId() + " removed");
            }
        }

        public void HandleUpdate(Data update)
        {
            foreach (Device device in DeviceMap.Values)
            {
                device.HandleUpdate(update);
            }
        }

        public void ScanThread()
        {
            while (IsScanning)
            {
                RetrieveDevice();
            }
        }

        public abstract void RetrieveDevice();

        public void StartScanning(int scan) {
            IsScanning = true;
            ScanningTime = scan;
            ScanningThread = new Thread(ScanThread);
            ScanningThread.IsBackground = true;
            ScanningThread.Start();
        }

        public abstract void OnNewDeviceFound(String id);



        public void OnDeviceDisconnect(string id)
        {
            // PreviousPort.Remove(port);
            System.Console.WriteLine(id + " Disconnect");
            RemoveDevice(id);
        }

    }
}
