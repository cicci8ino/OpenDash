﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OpenDash.Adapters;
using OpenDash.Adapters.rF2;
using OpenDash.Adapters.Delegates;
using OpenDash.DataStructure;
using OpenDash.Serial;
using OpenDash.Devices;
using OpenDash.Devices.Serial;
using OpenDash.Serial.Delegates;
using System.Diagnostics;
using OpenDash.Adapters;

namespace OpenDash
{
    public partial class MainForm : Form
    {

        //Adapter rF2Adapter;
        //AdapterManager am = new AdapterManager();
        //DeviceManager sm = new SerialManager();
        Controller controller;
        // Serial.SerialDevice display = new Serial.Display20x4("COM3", 9600);

        public MainForm()
        {
            InitializeComponent();
            
            //this.Show();
        }

        public void InitSDK()
        {
            controller = new Controller();
            controller.StartScanningForSim();
            controller.StartScanningForDevices(1000);
            controller.AddDeviceFoundHandler(new DeviceFoundHandler(OnNewDeviceFound));
            controller.AddSimFoundHandler(new SimFoundEventHandler(OnSimFound));
            SerialDevice gear = new GearIndicator("COM5", 9600);
            controller.ConnectToDevice(gear);
        }
        
        public void OnSimFound(object sender, EventArgs args)
        {
            BeginInvoke((Action)delegate ()
            {
                scanForSimLabel.Text = controller.GetSimName();
            });
        }


        public void ConnectionError(object sender, EventArgs args)
        {
            Debug.WriteLine("Cannot Connect");
        }

        public void ConnectionSucceded(object sender, EventArgs args)
        {
            Debug.WriteLine("Connection OK");

        }
        public void UpdatedValue(Data update)
        {
            controller.HandleUpdate(update);
            //display.addRPM(update.vehicle.engine.engineRPM);
           // display.addMaxRPM(update.vehicle.engine.engineRPMMax);
            //display.sendData();
            
        }

        public void Disconnected(object sender, EventArgs args)
        {
            Debug.WriteLine("Disconnected");
        }

        public void OnNewDeviceFound(String port)
        {
            while (!IsHandleCreated) { }  
            BeginInvoke((Action)delegate ()
            {
                this.checkedListBox1.Items.Insert(0, port);
            });
        
        }

        public void OnDisconnectedDevice(String port)
        {
            BeginInvoke((Action)delegate ()
            {
                this.checkedListBox1.Items.Remove(port);
            });
        }

        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //checkedListBox1.Ch
        }

    }
}
